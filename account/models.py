from django.db import models
from django.conf import settings


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    company_name = models.CharField(verbose_name='회사명',
                                    max_length=100, null=True, blank=True)
    company_number = models.CharField(verbose_name='사업자등록번호',
                                      max_length=50, null=True, blank=True)
    company_address = models.CharField(verbose_name='회사주소',
                                       max_length=50, null=True, blank=True)
    company_ceo_name = models.CharField(verbose_name='회사 대표자 성명',
                                        max_length=50, null=True, blank=True)
    company_contact = models.CharField(verbose_name='회사 전화',
                                       max_length=50, null=True, blank=True)
    mobile_phone = models.CharField(verbose_name='담당자 핸드폰',
                                    max_length=50)
    team_name = models.CharField(verbose_name='담당자 부서',
                                 max_length=50, null=True, blank=True)
    job_title = models.CharField(verbose_name='담당자 직책',
                                 max_length=50, null=True, blank=True)
    company_homepage = models.CharField(verbose_name='회사 홈페이지주소',
                                        max_length=50, null=True, blank=True)
    # date_of_birth = models.DateField(verbose_name='', blank=True, null=True)
    # photo = models.ImageField(upload_to='users/%Y/%m/%d', blank=True)

    USER_LEVEL_CHOICES = (
        ('rejected', '승인반려됨'),
        ('ongoing', '승인심사중'),
        ('client', '고객사'),
        ('reviewer', '리뷰어'),
        ('staff', '관리자'),
    )
    user_level = models.CharField(
        verbose_name='유저레벨', max_length=10, choices=USER_LEVEL_CHOICES,
        default='ongoing'
    )

    def __str__(self):
        return '{}님의 프로필'.format(self.user.username)
