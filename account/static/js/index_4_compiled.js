'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ButtonOffStyle;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**************
 Content Style
***************/
var Active = 'primary';
var Inactive = 'default';
var ButtonOnStyle = {
  width: '100%',
  borderColor: 'rgb(255, 209, 51)',
  backgroundColor: 'rgba(255, 209, 51, 0.7)',
  padding: 15,
  borderWidth: 1,
  borderTopStyle: 'solid',
  borderRightStyle: 'solid',
  borderBottomStyle: 'solid',
  borderLeftStyle: 'solid',
  textDecoration: 'none',
  fontSize: 16,
  color: '#000000',
  display: 'block'
};
var ButtonOffStyle = (_ButtonOffStyle = {
  width: '100%',
  borderColor: 'rgb(255, 209, 51)',
  padding: 15,
  borderWidth: 1,
  textDecoration: 'none',
  fontSize: 16
}, _defineProperty(_ButtonOffStyle, 'borderWidth', 1), _defineProperty(_ButtonOffStyle, 'borderTopStyle', 'solid'), _defineProperty(_ButtonOffStyle, 'borderRightStyle', 'solid'), _defineProperty(_ButtonOffStyle, 'borderBottomStyle', 'solid'), _defineProperty(_ButtonOffStyle, 'borderLeftStyle', 'solid'), _defineProperty(_ButtonOffStyle, 'color', '#888888'), _defineProperty(_ButtonOffStyle, 'backgroundColor', '#FFFFFF'), _defineProperty(_ButtonOffStyle, 'display', 'block'), _ButtonOffStyle);

var contentHeaderProblemStyle = {
  height: 30,
  padding: 3,
  backgroundColor: 'rgb(255, 209, 51)',
  textAlign: 'center',
  fontSize: '1.1em',
  fontStyle: 'bolder',
  color: '#555'
};

var contentHeaderSolutionStyle = contentHeaderProblemStyle;

var contentDivStyle = {
  height: 100,
  backgroundColor: '#FFF',
  width: '100%',
  display: 'table',
  fontSize: '0.9em'
};

var contentInnerDivStyle = {
  display: 'table-cell',
  verticalAlign: 'middle',
  textAlign: 'center',
  fontSize: '0.9em'
};

/****************
 react component
*****************/
var Content1 = React.createClass({
  displayName: 'Content1',

  getInitialState: function getInitialState() {
    return {
      button1Style: ButtonOnStyle,
      button2Style: ButtonOffStyle,
      contentId: '1'
    };
  },
  handleClick: function handleClick(e) {
    e.preventDefault();
    if (e.target.getAttribute("data-key") == '1') {
      this.setState({ button1Style: ButtonOnStyle,
        button2Style: ButtonOffStyle,
        contentId: '1'
      });
    } else {
      this.setState({ button1Style: ButtonOffStyle,
        button2Style: ButtonOnStyle,
        contentId: '2'
      });
    }
  },
  render: function render() {
    if (this.state.contentId == '1') {
      var content = React.createElement(ContentOne, null);
    } else {
      var content = React.createElement(ContentTwo, null);
    }
    var containerDivStyle = { marginBottom: 20 };
    // var buttonStyle = {height: 20 }
    return React.createElement(
      'div',
      { className: 'row' },
      React.createElement(
        'div',
        { className: 'small-3 large-3 columns' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            null,
            React.createElement(
              'a',
              {
                style: this.state.button1Style,
                'data-key': '1', href: '#',
                onClick: this.handleClick
              },
              '신규 / 버티컬\u2028 »'
            )
          ),
          React.createElement(
            'div',
            null,
            React.createElement(
              'a',
              {
                style: this.state.button2Style,
                'data-key': '2', href: '#',
                onClick: this.handleClick
              },
              '대형 업체 »'
            )
          )
        )
      ),
      React.createElement(
        'div',
        { className: 'small-9 large-9 columns' },
        content
      )
    );
  }
});

var ContentOne = React.createClass({
  displayName: 'ContentOne',

  render: function render() {
    return React.createElement(
      'div',
      { className: 'row' },
      React.createElement(
        'div',
        { className: 'small-12 large-6 columns' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { style: contentHeaderProblemStyle },
            React.createElement('i', { className: 'fa fa-exclamation-circle', 'aria-hidden': 'true' }),
            ' ',
            '고민'
          ),
          React.createElement(
            'div',
            { style: contentDivStyle },
            React.createElement(
              'div',
              { style: contentInnerDivStyle },
              '광고심의 업무 인력 부재에 따른',
              React.createElement('br', null),
              '표시 · 광고 관련 법령 위반 리스크',
              React.createElement('br', null),
              '법 위반에 따른 규제기관 행정처분',
              React.createElement('br', null),
              '부정 언론보도 확산 및 브랜드 이미지 타격',
              React.createElement('br', null)
            )
          )
        )
      ),
      React.createElement(
        'div',
        { className: 'small-12 large-6 columns' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { style: contentHeaderSolutionStyle },
            React.createElement('i', { className: 'fa fa-check-circle', 'aria-hidden': 'true' }),
            ' ',
            '해결 방안'
          ),
          React.createElement(
            'div',
            { style: contentDivStyle },
            React.createElement(
              'div',
              { style: contentInnerDivStyle },
              '광고심의 프로세스 및 어드민 시스템 컨설팅',
              React.createElement('br', null),
              '고객사 교육 및 내부인력 양성 지원',
              React.createElement('br', null),
              '광고심의 업무 대행',
              React.createElement('br', null)
            )
          )
        )
      )
    );
  }
});

var ContentTwo = React.createClass({
  displayName: 'ContentTwo',

  render: function render() {
    return React.createElement(
      'div',
      { className: 'row' },
      React.createElement(
        'div',
        { className: 'small-12 large-6 columns' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { style: contentHeaderProblemStyle },
            React.createElement('i', { className: 'fa fa-exclamation-circle', 'aria-hidden': 'true' }),
            ' ',
            '고민'
          ),
          React.createElement(
            'div',
            { style: contentDivStyle },
            React.createElement(
              'div',
              { style: contentInnerDivStyle },
              '대규모 인력 채용, 고용유지에 따른 직간접적 비용 문제',
              React.createElement('br', null),
              '일간/주간 신규 판매상품 숫자 편차에 따른 인력 운영 이슈',
              React.createElement('br', null)
            )
          )
        )
      ),
      React.createElement(
        'div',
        { className: 'small-12 large-6 columns' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { style: contentHeaderSolutionStyle },
            React.createElement('i', { className: 'fa fa-check-circle', 'aria-hidden': 'true' }),
            ' ',
            '해결 방안'
          ),
          React.createElement(
            'div',
            { style: contentDivStyle },
            React.createElement(
              'div',
              { style: contentInnerDivStyle },
              '종합몰, 소셜커머스 등',
              React.createElement('br', null),
              '대형 통신판매업체 광고심의 업무대행 아웃소싱'
            )
          )
        )
      )
    );
  }
});

ReactDOM.render(React.createElement(Content1, null), document.getElementById('content1'));

var Values = function (_React$Component) {
  _inherits(Values, _React$Component);

  function Values() {
    _classCallCheck(this, Values);

    return _possibleConstructorReturn(this, Object.getPrototypeOf(Values).apply(this, arguments));
  }

  _createClass(Values, [{
    key: 'render',
    value: function render() {
      var valueContents = [{
        // icon: 'fa-crosshairs',
        icon: 'fa-search',
        title: '정확성',
        line1: '표시 · 광고 규제 준수를 위한 리스크 진단',
        line2: '컨설팅 및 광고심의 업무 대행 서비스'
      }, {
        icon: 'fa-krw',
        title: '절약',
        line1: '추가인력채용 / 유지 비용 절약',
        line2: '신속한 온디멘드 심의로 시간 절약'
      }, {
        icon: 'fa-line-chart',
        title: '경쟁력 향상',
        line1: '고객사의 규제 대응 능력을 통한 리스크 관리',
        line2: '기업 경쟁력 향상 도모'
      }];

      var panelStyle = {
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        border: '1px solid rgb(255, 209, 51)',
        paddingTop: '1em',
        paddingBottom: '1em',
        display: 'block',
        textAlign: 'center'
      };
      var iconStyle = {
        color: 'rgb(255, 209, 51)',
        paddingTop: 20,
        paddingBottom: 20,
        display: 'block',
        textAlign: 'center',
        fontSize: '2em'
      };
      var titleStyle = {
        // paddingTop: 50,
        paddingBottom: 20,
        fontSize: '1.2em',
        display: 'block',
        textAlign: 'center',
        color: '#777'
      };
      var lineStyle = {
        // paddingTop: 50,
        // paddingBottom: 20,
        height: '1.8em',
        fontSize: '0.9em',
        display: 'block',
        textAlign: 'center'
      };

      var values = valueContents.map(function (content, i) {
        var iconClass = 'fa ' + content.icon + ' fa-5x text-center';
        return React.createElement(
          'div',
          { className: 'small-4 large-4 columns',
            key: i,
            style: panelStyle
          },
          React.createElement('i', { style: iconStyle, className: iconClass, 'aria-hidden': 'true' }),
          React.createElement(
            'span',
            { style: titleStyle },
            content.title
          ),
          React.createElement(
            'p',
            { style: lineStyle },
            content.line1
          ),
          React.createElement(
            'p',
            { style: lineStyle },
            content.line2
          )
        );
      });
      return React.createElement(
        'div',
        { className: 'row' },
        values
      );
    }
  }]);

  return Values;
}(React.Component);

ReactDOM.render(React.createElement(Values, null), document.getElementById('content2'));