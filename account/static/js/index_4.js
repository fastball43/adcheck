/**************
 Content Style
***************/
var Active = 'primary';
var Inactive = 'default';
var ButtonOnStyle = {
    width: '100%',
    borderColor: 'rgb(255, 209, 51)',
    backgroundColor: 'rgba(255, 209, 51, 0.7)',
    padding: 15,
    borderWidth: 1,
    borderTopStyle: 'solid',
    borderRightStyle: 'solid',
    borderBottomStyle: 'solid',
    borderLeftStyle: 'solid',
    textDecoration: 'none',
    fontSize: 16,
    color: '#000000',
    display: 'block',
};
var ButtonOffStyle = {
    width: '100%',
    borderColor: 'rgb(255, 209, 51)',
    padding: 15,
    borderWidth: 1,
    textDecoration: 'none',
    fontSize: 16,
    borderWidth: 1,
    borderTopStyle: 'solid',
    borderRightStyle: 'solid',
    borderBottomStyle: 'solid',
    borderLeftStyle: 'solid',
    color: '#888888',
    backgroundColor: '#FFFFFF',
    display: 'block',
}

var contentHeaderProblemStyle = {
  height: 30,
  padding: 3,
  backgroundColor: 'rgb(255, 209, 51)',
  textAlign: 'center',
  fontSize: '1.1em',
  fontStyle: 'bolder',
  color: '#555',
};

var contentHeaderSolutionStyle = contentHeaderProblemStyle

var contentDivStyle = {
  height: 100,
  backgroundColor: '#FFF',
  width: '100%',
  display: 'table',
  fontSize: '0.9em',
};

var contentInnerDivStyle = {
  display: 'table-cell',
  verticalAlign: 'middle',
  textAlign: 'center',
  fontSize: '0.9em',
}


/****************
 react component
*****************/
var Content1 = React.createClass({
  getInitialState: function() {
    return {
      button1Style: ButtonOnStyle,
      button2Style: ButtonOffStyle,
      contentId: '1'
    }
  },
  handleClick: function(e) {
    e.preventDefault();
    if (e.target.getAttribute("data-key") == '1') {
      this.setState({button1Style: ButtonOnStyle,
                     button2Style: ButtonOffStyle,
                     contentId: '1'
                   });
    } else {
      this.setState({button1Style: ButtonOffStyle,
                     button2Style: ButtonOnStyle,
                     contentId: '2'
                   });
    }
  },
  render: function() {
    if (this.state.contentId == '1') {
      var content = <ContentOne />;
    } else {
      var content = <ContentTwo />;
    }
    var containerDivStyle = {marginBottom: 20}
    // var buttonStyle = {height: 20 }
    return (
      <div className="row">
        <div className="small-3 large-3 columns">
          <div
            // style={containerDivStyle}
            // className="small-12 columns"
          >
            <div
            >
              <a
                style={this.state.button1Style}
                data-key='1' href="#"
                onClick={this.handleClick}
                >
                신규 / 버티컬  &raquo;
              </a>
            </div>
            <div
            >
              <a
                style={this.state.button2Style}
                data-key='2' href="#"
                onClick={this.handleClick}
                >
                대형 업체 &raquo;
              </a>
            </div>
          </div>
        </div>
        <div className="small-9 large-9 columns">
          {content}
        </div>
      </div>
    );
  }
});

var ContentOne = React.createClass({
  render: function() {
    return (
      <div className="row">
        <div className="small-12 large-6 columns">
          <div>
            <div style={contentHeaderProblemStyle}>
              <i className="fa fa-exclamation-circle" aria-hidden="true"></i>
              {' '}고민
            </div>
            <div style={contentDivStyle}>
              <div style={contentInnerDivStyle}>
                광고심의 업무 인력 부재에 따른<br />
                표시 · 광고 관련 법령 위반 리스크<br />
                법 위반에 따른 규제기관 행정처분<br />
                부정 언론보도 확산 및 브랜드 이미지 타격<br />
              </div>
            </div>
          </div>
        </div>
        <div className="small-12 large-6 columns">
          <div>
            <div style={contentHeaderSolutionStyle}>
              <i className="fa fa-check-circle" aria-hidden="true"></i>
              {' '}해결 방안
            </div>
            <div style={contentDivStyle}>
              <div style={contentInnerDivStyle}>
                광고심의 프로세스 및 어드민 시스템 컨설팅<br />
                고객사 교육 및 내부인력 양성 지원<br />
                광고심의 업무 대행<br />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var ContentTwo = React.createClass({
  render: function() {
    return (
      <div className="row">
        <div className="small-12 large-6 columns">
          <div>
            <div style={contentHeaderProblemStyle}>
              <i className="fa fa-exclamation-circle" aria-hidden="true"></i>
              {' '}고민
            </div>
            <div style={contentDivStyle}>
              <div style={contentInnerDivStyle}>
                대규모 인력 채용, 고용유지에 따른 직간접적 비용 문제<br />
                일간/주간 신규 판매상품 숫자 편차에 따른 인력 운영 이슈<br />
              </div>
            </div>
          </div>
        </div>
        <div className="small-12 large-6 columns">
          <div>
            <div style={contentHeaderSolutionStyle}>
              <i className="fa fa-check-circle" aria-hidden="true"></i>
              {' '}해결 방안
            </div>
            <div style={contentDivStyle}>
                <div style={contentInnerDivStyle}>
                  종합몰, 소셜커머스 등<br />
                  대형 통신판매업체 광고심의 업무대행 아웃소싱
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

ReactDOM.render(
  <Content1 />,
  document.getElementById('content1')
);



class Values extends React.Component {
  render() {
    const valueContents = [
      {
        // icon: 'fa-crosshairs',
        icon: 'fa-search',
        title: '정확성',
        line1: '표시 · 광고 규제 준수를 위한 리스크 진단',
        line2: '컨설팅 및 광고심의 업무 대행 서비스'
      },
      {
        icon: 'fa-krw',
        title: '절약',
        line1: '추가인력채용 / 유지 비용 절약',
        line2: '신속한 온디멘드 심의로 시간 절약'
      },
      {
        icon: 'fa-line-chart',
        title: '경쟁력 향상',
        line1: '고객사의 규제 대응 능력을 통한 리스크 관리',
        line2: '기업 경쟁력 향상 도모'
      },
    ];

    const panelStyle = {
      backgroundColor: 'rgba(255, 255, 255, 0.9)',
      border: '1px solid rgb(255, 209, 51)',
      paddingTop: '1em',
      paddingBottom: '1em',
      display: 'block',
      textAlign: 'center',
    }
    const iconStyle = {
      color: 'rgb(255, 209, 51)',
      paddingTop: 20,
      paddingBottom: 20,
      display: 'block',
      textAlign: 'center',
      fontSize: '2em',
    }
    const titleStyle = {
      // paddingTop: 50,
      paddingBottom: 20,
      fontSize: '1.2em',
      display: 'block',
      textAlign: 'center',
      color: '#777'
    }
    const lineStyle = {
      // paddingTop: 50,
      // paddingBottom: 20,
      height: '1.8em',
      fontSize: '0.9em',
      display: 'block',
      textAlign: 'center',
    }


    let values = valueContents.map((content, i) => {
      let iconClass = `fa ${content.icon} fa-5x text-center`;
      return (
        <div className='small-4 large-4 columns'
            key={i}
            style={panelStyle}
        >
          <i style={iconStyle} className={iconClass} aria-hidden="true"></i>
          <span style={titleStyle}>{content.title}</span>
          <p style={lineStyle}>{content.line1}</p>
          <p style={lineStyle}>{content.line2}</p>
        </div>
      );
    });
    return (
      <div className='row'>
        {/*
          <h1 className='text-center'>Values</h1>
        */}
        {values}
      </div>
    )
  }
}

ReactDOM.render(
  <Values />,
  document.getElementById('content2')
);