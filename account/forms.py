from django import forms
from django.contrib.auth.models import User
from .models import Profile
from django.utils.translation import ugettext_lazy as _


# class LoginForm(forms.Form):
#     username = forms.CharField(label='아이디 또는 이메일주소')
#     password = forms.CharField(widget=forms.PasswordInput)

class RegistrationServiceAgreementForm(forms.Form):
    agree_service = forms.BooleanField(label='이용약관에 동의합니다.',
                                       required=False)

    def clean_agree_service(self):
        cd = self.cleaned_data
        if cd['agree_service'] is False:
            raise forms.ValidationError('서비스 이용약관에 동의해 주세요.')
        return cd['agree_service']


class RegistrationPolicyAgreementForm(forms.Form):
    agree_policy = forms.BooleanField(label='개인정보 수집 및 이용에 동의합니다.',
                                      required=False)

    def clean_agree_policy(self):
        cd = self.cleaned_data
        if cd['agree_policy'] is False:
            raise forms.ValidationError('개인정보 수집 및 이용에 동의해 주세요.')
        return cd['agree_policy']


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='비밀번호(필수)',
                               widget=forms.PasswordInput)
    password2 = forms.CharField(label='비밀번호 확인(필수)',
                                widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')
        labels = {
            'username': _('아이디(필수):'),
            'first_name': _('성명(필수):'),
            'email': _('이메일주소(필수):')
        }

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('비밀번호가 일치하지 않습니다.')
        return cd['password2']


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        # fields = ('first_name', 'last_name', 'email')
        fields = ('first_name', 'email')


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            'mobile_phone',
            'company_name',
            'company_number',
            'company_address',
            'company_ceo_name',
            'job_title',
            'team_name',
            'company_contact',
            'company_homepage'
        )
        labels = {
            'mobile_phone': _('이동전화(필수)'),
            'company_name': _('회사명(광고주 회원 필수)'),
            'company_number': _('사업자등록번호(광고주 회원 필수)'),
            'company_address': _('회사 주소(광고주 회원 필수)'),
            'company_ceo_name': _('대표 성명(광고주 회원 필수)'),
            'job_title': _('직책'),
            'team_name': _('부서'),
            'company_contact': _('회사 전화'),
            'company_homepage': _('회사 홈페이지 주소')
        }
