from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from .forms import RegistrationServiceAgreementForm
from .forms import RegistrationPolicyAgreementForm
from .models import Profile


def register_agree(request):
    if request.method == 'POST':
        print(request)
        service_agree_form = RegistrationServiceAgreementForm(request.POST)
        policy_agree_form = RegistrationPolicyAgreementForm(request.POST)
        if service_agree_form.is_valid() and policy_agree_form.is_valid():
            user_form = UserRegistrationForm()
            profile_form = ProfileEditForm()
            return render(request,
                          'account/register.html',
                          {'user_form': user_form,
                           'agreed': True,
                           'profile_form': profile_form})
            # return redirect('register', agreed='True')
    else:
        service_agree_form = RegistrationServiceAgreementForm()
        policy_agree_form = RegistrationPolicyAgreementForm()
    return render(request,
                  'account/register_agree.html',
                  {'service_agree_form': service_agree_form,
                   'policy_agree_form': policy_agree_form})


def register(request):
    if request.method == 'POST':
        # agreed = request.agreed
        user_form = UserRegistrationForm(request.POST)
        profile_form = ProfileEditForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            # 신규 유저 오프젝트를 만드나 저장하지는 앖음
            new_user = user_form.save(commit=False)
            # 비번 저장
            new_user.set_password(
                user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()

            # create the user profile
            profile = profile_form.save(commit=False)
            profile.user_id = new_user.id
            profile = profile_form.save()
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
        else:
            return render(request,
                          'account/register.html',
                          {'user_form': user_form,
                           'agreed': True,
                           'profile_form': profile_form})
    else:
        user_form = UserRegistrationForm()
        profile_form = ProfileEditForm()
        service_agree_form = RegistrationServiceAgreementForm()
        policy_agree_form = RegistrationPolicyAgreementForm()

    return render(request,
                  'account/register.html',
                  {'user_form': user_form,
                   'service_agree_form': service_agree_form,
                   'policy_agree_form': policy_agree_form,
                   'profile_form': profile_form})


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,
                                       data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,
                  'account/edit.html',
                  {'user_form': user_form,
                   'profile_form': profile_form})


@login_required
def dashboard(request):
    # 모델 객체의 모든 필드 조회하기
    # print(request.user._meta.get_all_field_names())
    # 모델 객체의 모든 함수 조회하기
    # print(dir(request.user))
    return render(request,
                  'account/dashboard.html',
                  {'section': 'dashboard'})


# from django.http import HttpResponse
# from django.contrib.auth import authenticate, login
# from .forms import LoginForm
# def user_login(request):
#     if request.method == 'POST':
#         form = LoginForm(request.POST)
#         if form.is_valid():
#             cd = form.cleaned_data
#             user = authenticate(username=cd['username'],
#                                 password=cd['password'])
#             if user is not None:
#                 if user.is_active:
#                     login(request, user)
#                     return HttpResponse('authencicate')
#                 else:
#                     return HttpResponse('Disabled account')
#             else:
#                 return HttpResponse('invalid login')
#     else:
#         form = LoginForm()
#     return render(request, 'account/login.html', {'form': form})
