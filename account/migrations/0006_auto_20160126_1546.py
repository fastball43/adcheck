# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_auto_20160126_1539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='user_level',
            field=models.CharField(verbose_name='유저레벨', choices=[('rejected', '승인반려됨'), ('ongoing', '승인심사중'), ('client', '고객사'), ('reviewer', '리뷰어'), ('staff', '관리자')], max_length=10, default='ongoing'),
        ),
    ]
