# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20160126_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='company_address',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='회사주소'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_ceo_name',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='회사 대표자 성명'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_contact',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='회사 전화'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_homepage',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='회사 홈페이지주소'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_name',
            field=models.CharField(blank=True, null=True, max_length=100, verbose_name='회사명'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_number',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='사업자등록번호'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='job_title',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='담당자 직책'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='mobile_phone',
            field=models.CharField(max_length=50, verbose_name='담당자 핸드폰'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='team_name',
            field=models.CharField(blank=True, null=True, max_length=50, verbose_name='담당자 부서'),
        ),
    ]
