# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_profile_user_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='user_level',
            field=models.CharField(choices=[('rejected', '승인반려됨'), ('ongoing', '승인심사중'), ('client', '고객사'), ('reviwer', '리뷰어'), ('staff', '관리자')], default='ongoing', max_length=10, verbose_name='유저레벨'),
        ),
    ]
