# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='date_of_birth',
        ),
        migrations.AddField(
            model_name='profile',
            name='company_name',
            field=models.CharField(max_length=100, blank=True, verbose_name='회사명', null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='job_title',
            field=models.CharField(max_length=50, blank=True, verbose_name='직함', null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='mobile_phone',
            field=models.CharField(max_length=50, blank=True, verbose_name='연락처', null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='team_name',
            field=models.CharField(max_length=50, blank=True, verbose_name='부서명', null=True),
        ),
    ]
