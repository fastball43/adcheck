# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20160114_1921'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='company_address',
            field=models.CharField(verbose_name='회사주소(필수)', default='default', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='company_ceo_name',
            field=models.CharField(verbose_name='대표자 성명(필수)', default='default', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='company_contact',
            field=models.CharField(null=True, verbose_name='대표전화', blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='profile',
            name='company_homepage',
            field=models.CharField(null=True, verbose_name='홈페이지주소', blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='profile',
            name='company_number',
            field=models.CharField(verbose_name='사업자등록번호(필수)', default='dafault', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='company_name',
            field=models.CharField(verbose_name='회사명(필수)', default='default', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='job_title',
            field=models.CharField(verbose_name='직책(필수)', default='dafault', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='mobile_phone',
            field=models.CharField(verbose_name='담당자 연락처(필수)', default='default', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='team_name',
            field=models.CharField(verbose_name='부서(필수)', default='default', max_length=50),
            preserve_default=False,
        ),
    ]
