from django.contrib import admin
from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
    '''
        Admin View for Profile
    '''
    list_display = ['user_level', 'user', 'mobile_phone', 'company_name',
                    'team_name', 'job_title']
    list_filter = ['user_level']

admin.site.register(Profile, ProfileAdmin)
