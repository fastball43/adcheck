// 시작
  // var CommentBox = React.createClass({
  //     loadCommentsFromServer: function() {
  //         $.ajax({
  //             url: this.props.url,
  //             dataType: 'json',
  //             cache: false,
  //             success: function(data) {
  //                 this.setState({data: data});
  //                 // console.log('loadCommentsFromServer: success');
  //             }.bind(this),
  //             error: function(xhr, status, err) {
  //                 console.error(this.props.url, status, err.toString());
  //             }.bind(this)
  //         });
  //         // console.log('loadCommentsFromServer: completed');
  //     },
  //     getInitialState: function() {
  //         return {data: []};
  //     },
  //     componentDidMount: function() {
  //         // console.log('commponentDidMount');
  //         this.loadCommentsFromServer();
  //         // setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  //     },
  //     render: function() {
  //         return (
  //             <div className="panel callout">
  //                 <h4>댓글들</h4>
  //                 <CommentList data={this.state.data} />
  //                 <CommentForm />
  //             </div>
  //         );
  //     }
  // });
  //
  // var CommentList = React.createClass({
  //     render: function() {
  //         // console.log('this.props.data');
  //         // console.log(this.props.data);
  //         var commentNodes = this.props.data.map( function (comment) {
  //             return (
  //                 <Comment author={comment.owner} key={comment.id} id={comment.id}>
  //                     {comment.text}
  //                 </Comment>
  //             );
  //         });
  //         return (
  //             <div className="commentList">
  //                 {commentNodes}
  //             </div>
  //         );
  //     }
  // });
  //
  // var CommentForm = React.createClass({
  //     render: function() {
  //         return (
  //             <div className="commentForm">
  //                 저는 코멘트 폼이에요
  //             </div>
  //         );
  //     }
  // });
  //
  // var Comment = React.createClass({
  //     rawMarkup: function() {
  //         var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
  //         return { __html: rawMarkup };
  //     },
  //
  //     render: function() {
  //         return (
  //             <div className="panel">
  //                 <p className="label">
  //                     {this.props.author}
  //                 </p>
  //                 <p>key: {this.props.id}</p>
  //                 <span dangerouslySetInnerHTML={this.rawMarkup()} />
  //             </div>
  //         );
  //     }
  // });
  // ReactDOM.render(
  //     <CommentBox url="/api/image_comments.json" />,
  //     document.getElementById('commentBox')
  // );
//

var CommentBox = React.createClass({
  handleCommentSubmit: function (comment) {
    // alert('submit 버튼이 눌렸습니다.')
    // console.log(comment.author);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function (data) {
        this.setState({data: data});
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this),
    });
  },
  render: function () {
    return (
      <div className="small-12 columns">
        <h1>코멘트 박스</h1>
        <CommentList comments={this.props.comments}/>
        <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
      </div>
    );
  }
});
var data = [
  {id: 1, author: "홍길동", text: "<b>안녕</b>하세요."},
  {id: 2, author: "하지무", text: "hello there?"}
]
var CommentList = React.createClass({
  render: function () {
    var counter = this.props.comments.length;
    var commentNodes = this.props.comments.map(function(comment) {
      return (
        <Comment author={comment.author} key={comment.id}>
          {comment.text}
        </Comment>
      );
    });
    return (
      <div className="commentList">
        댓글의 갯수는 {counter}개 입니다.
        {commentNodes}
      </div>
    );
  }
});
var Comment = React.createClass({
  render: function () {
    return (
      <div className="comment">
        <h2 className="label">
          {this.props.author}
        </h2>
        {this.props.children}
      </div>
    );
  }
});
var CommentForm = React.createClass({
  getInitialState: function () {
    return {author: '', text: '', height: ''};
  },
  handleAuthorChange: function (e) {
    this.setState({author: e.target.value});
  },
  handleTextChange: function (e) {
    this.setState({text: e.target.value});
    console.log(e);
    console.log('height: '+e.target.style.height);
    // this.setState({height: (20+e.scrollHeight)
    // e.style.height = "1px";
    // e.style.height = (20+e.scrollHeight)+"px";
    // alert('수정이 불가능한 필드입니다.');
  },
  handleSubmit: function (e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    if (!text || !author) {
      alert('내용을 입력해 주세요');
      return;
    }
    this.props.onCommentSubmit({author: author, text: text})
    this.setState({author: '', text: ''});
  },
  render: function () {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="이름을 입력해 주세요."
          value={this.state.author}
          onChange={this.handleAuthorChange}
        />
        <textarea
          type="text"
          placeholder="댓글을 입력해 주세요."
          value={this.state.text}
          onChange={this.handleTextChange}
        />
        <input type="submit" className="button" value="입력하기" />
      </form>
    );
  }
});
// ReactDOM.render(
//   <CommentBox comments={data}/>,
//   document.getElementById('commentBox')
// );

class Hello extends React.Component {
    render () {
        return <div>Heeeello</div>;
    }
}
// class Hello extends React.Component {
//     helloClicker (e) {
//         var targ = e.target;
//         targ.textContent = this.helloText(targ.textContent);
//     }
//     helloText (text) {
//         return text === 'Hello' ? 'World' : 'Hello';
//     }
//     render () {
//         return <div onClick={this.helloClicker}>Hello</div>;
//     }
// }
// ReactDOM.render(
//     <Hello />,
//     document.getElementById('hello')
// );

var WhatTime = React.createClass({
  render: function () {
    return (
      <p>
        지금 시간은 {this.props.date.toTimeString()} 입니다.
      </p>
    );
  }
});
// setInterval(function () {
//   ReactDOM.render(
//     <WhatTime date={new Date()} />,
//     document.getElementById("whatTime")
//   );
// }, 500);

var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
