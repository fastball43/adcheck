from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    # url(r'^test/$', views.test, name='test'),
    url(r'^items/$', views.ItemList.as_view()),
    url(r'^items/(?P<pk>[0-9]+)$', views.ItemDetail.as_view()),
    url(r'^images/$', views.ImageList.as_view()),
    # url(r'^images/(?P<pk>[0-9]+)$', views.ItemImageList.as_view()),
    url(r'^items/(?P<pk>[0-9]+)/images/$', views.ItemImageList.as_view()),
    url(r'^images/(?P<pk>[0-9]+)$', views.ImageDetail.as_view()),
    url(r'^image_comments/$', views.ImageCommentList.as_view()),
    url(r'^image_comments/(?P<pk>[0-9]+)$',
        views.ImageCommentDetail.as_view()),
    # 아래 Comments/num 은 구버전임
    url(r'^comments/(?P<pk>[0-9]+)$', views.CommentList.as_view()),
    # 아래로 대치하여야 함.
    url(r'^images/(?P<pk>[0-9]+)/comments$', views.CommentList.as_view()),
    url(r'^attach_file/(?P<pk>[0-9]+)$', views.AttachFileDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
