from item.models import Item, Image, ImageComment, AttachFile
from api.serializers import ItemSerializer, ImageSerializer,\
                            ImageCommentSerializer, ItemImageSerializer
from rest_framework import generics, permissions
from .permissions import IsOwnerOrReadOnly

from django.shortcuts import render


class ItemList(generics.ListCreateAPIView):
    """
    아이템리스트 조회 수정
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class ItemDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    개별 아이템 조회, 수정, 삭제
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class ImageList(generics.ListCreateAPIView):
    """
    리스트 조회 수정
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    아이템 이미지의 개별 조회, 수정, 삭제
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class AttachFileDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    아이템 첨부파일의 개별 조회, 수정, 삭제
    """
    queryset = AttachFile.objects.all()
    serializer_class = ItemImageSerializer


class ImageCommentList(generics.ListAPIView):
    """
    리스트 조회 수정
    """
    queryset = ImageComment.objects.all()
    serializer_class = ImageCommentSerializer


class ImageCommentDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    개별 조회, 수정, 삭제
    """
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly,)
    queryset = ImageComment.objects.all()
    serializer_class = ImageCommentSerializer


def test(request):
    context = {}
    return render(request, 'api/test.html', context)


class CommentList(generics.ListAPIView):
    """
    리스트 조회 수정
    """
    serializer_class = ImageCommentSerializer

    def get_queryset(self):
        """
        이미지 id에 해당하는 comments만 리턴
        """
        image = self.kwargs['pk']
        return ImageComment.objects.filter(image=image)


class ItemImageList(generics.ListAPIView):
    """
    아이템 번호로 속해있는 이미지들을 조회한다.
    """
    serializer_class = ItemImageSerializer

    def get_queryset(self):
        """
        Item id에 해당하는 comments만 리턴
        """
        item = self.kwargs['pk']
        return Image.objects.filter(item=item)
