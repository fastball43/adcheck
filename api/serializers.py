from item.models import Item, Image, ImageComment, AttachFile
from rest_framework import serializers


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'title', 'user', 'created',
                  'modified', 'description', 'url', 'status')


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'item', 'comments')


class AttachFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttachFile
        fields = ('id', 'file')


class ItemImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'image', 'order')


class ImageCommentSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = ImageComment
        fields = ('id', 'owner', 'image', 'text', 'created', 'modified')
