from .base import *
import custom_storages


STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'custom_storages.StaticStorage'
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
MEDIA_ROOT = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)


DEBUG = False

ADMINS = (
    ('Peter Park', 'fastball43@gmail.com'),
)

ALLOWED_HOSTS = [
    'www.adcheck.kr',
    'adcheck.kr'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'adcheck',
        'USER': 'fastball43',
        'PASSWORD': 'Peter1!park',
        'HOST': 'adcheck.cxgxhtryyypy.ap-northeast-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}

