"""adcheck URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^doggy_admin/', include(admin.site.urls)),
    url(r'^accounts/', include('account.urls')),
    url(r'^item/', include('item.urls', namespace="item")),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^$',
        # TemplateView.as_view(template_name="index.html"),
        TemplateView.as_view(template_name="index_ver_4.html"),
        # TemplateView.as_view(template_name="index_pr.html"),
        # TemplateView.as_view(template_name="index_ver_3.html"),
        name='home'
        ),
    url(r'^old/',
        TemplateView.as_view(template_name="index_pr.html"),
        # TemplateView.as_view(template_name="index_ver_4.html")
        ),
    url(r'^file_upload/', include('django_file_form.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
