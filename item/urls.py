from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^new/$', views.new, name='new'),
    url(r'^(?P<item_id>\d+)$', views.end, name='detail'),
    # url(r'^(?P<item_id>\d+)$', views.detail, name='detail'),
    # url(r'^end/(?P<item_id>\d+)$', views.end, name='end'),
    url(r'^end/add_image/(?P<item_id>\d+)$',
        views.end_addimages, name='end_addimages'),
    url(r'^end/add_files/(?P<item_id>\d+)$',
        views.end_addfiles, name='end_addfiles'),
    url(r'^add_comment$', views.add_comment, name='add_comment'),
    url(r'^(?P<item_id>\d+)/images/$',
        views.end_image_order, name='end_image_order'),
    url(r'^review$', views.review_list, name='review_list'),
    url(r'^review_update/(?P<item_id>\d+)$',
        views.review_update, name='review_update'),
    url(r'^history/$',
        views.history, name='history'),
    url(r'^review_complete/(?P<item_id>\d+)/(?P<status_code>\d+)$',
        views.review_complete, name='review_complete'),
]
