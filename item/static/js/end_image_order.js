
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    // return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    return (/^(HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var ReduxThunk = window.ReduxThunk.default



/***************
 * action types
 ***************/
const GO_UP = 'GO_UP'
const GO_DOWN = 'GO_DOWN'
const GO_TO = 'GO_TO'
const SET_ITEM_ID = 'SET_ITEM_ID'
const RECEIVE_IMAGES = 'RECEIVE_IMAGES'
const SAVE_ORDER = 'SAVE_ORDER'


/******************
 * action creators
******************/
function receiveImages(data) {
  return { type: RECEIVE_IMAGES, data }
}

function goUp(index) {
  return { type: GO_UP, index }
}

function goDown(index) {
  return { type: GO_DOWN, index }
}

function goTo(index, to) {
  return { type: GO_TO, index, to }
}

function setItemId(id) {
  return { type: SET_ITEM_ID, id }
}

function saveOrder(imageOrders) {
  return { type: SAVE_ORDER, imageOrders }
}


/***********
 * Reducers
 ***********/
function _old_imageOrderApp(state = {}, action) {
  return {
    itemId: itemId(state.itemId, action),
    images: imageList(state.images, action)
  }
  // switch (action.type) {
  //   case SET_ITEM_ID:
  //     return Object.assign({}, state, {
  //       itemId: action.id
  //     })
  //   case GO_UP:
  //   case GO_DOWN:
  //   case GO_TO:
  //     return Object.assign({}, state, {
  //       images: imaseList(state.images, action)
  //     })
  //   default:
  //     return state
  // }
}

function itemId(state = 0, action) {
  switch (action.type) {
    case SET_ITEM_ID:
      return action.id
    default:
      return state
  }
}

function imageList(state = [], action) {
  switch (action.type) {
    case RECEIVE_IMAGES:
      return Object.assign([], action.data )
    case GO_UP:
      // 옮기려는 이미지의 인덱스가 벗어났는가?
      if ((action.index <= 1) || (action.index > state.length)) {
        return state
      }
      var nextState = Object.assign([], state);
      nextState.splice((action.index - 2), 0, nextState.splice((action.index -1), 1)[0]);
      return nextState

    case GO_DOWN:
      // 옮기려는 이미지의 인덱스가 벗어났는가?
      if ((action.index <= 0) || (action.index >= state.length)) {
        return state
      }
      var nextState = Object.assign([], state)
      nextState.splice((action.index), 0, nextState.splice((action.index -1), 1)[0])
      return nextState

    case GO_TO:
      // 옮기려는 이미지의 인덱스가 벗어났는가?
      if ((action.index <= 0) || (action.index > state.length)) {
        return state
      }
      // 옮길 위치가 이상한가?
      if (action.to <= 0) {
        return state
      }
      var nextState = Object.assign([], state)
      nextState.splice((action.to -1), 0, nextState.splice((action.index -1), 1)[0])
      return nextState

    default:
      return state
  }
}


const imageOrderApp = Redux.combineReducers({
  itemId: itemId,
  images: imageList,
})




/************
 * 테스트 영역
 ************/

// console.time("dispatch debug")
// store.dispatch(goUp(1));
// store.dispatch(goUp(100));
// store.dispatch(goDown(100));
// store.dispatch(goDown(0));
// store.dispatch(goDown(1));
// store.dispatch(goTo(-10, 12));
// store.dispatch(goTo(12, 3));
// store.dispatch(goTo(12, 3));
// store.dispatch(goTo(3, 11))
// store.dispatch(goTo(3, -1))
// store.dispatch(goUp(3))
// store.dispatch(setItemId(21))
// store.dispatch({type: 'GO_TO', index: 10, to: 1})
// console.timeEnd("dispatch debug")
// unsubscribe()


/*
 * Presentational Components
 */

class UpButton extends React.Component {
  render () {
    const { index } = this.props
    return (
      <div
        className='tiny expand button'
        // onClick={() => {console.log(index);}}
        onClick={() => {
          store.dispatch(goUp(index))
        }}
      >
        ▲ 위로 이동
      </div>
    )
  }
}
UpButton.propTypes = {
  index: React.PropTypes.number.isRequired
}
UpButton = ReactRedux.connect()(UpButton)

class DownButton extends React.Component {
  render () {
    const { index } = this.props
    return (
      <div
        className='tiny expand button'
        // onClick={() => {console.log(index);}}
        onClick={() => {
          store.dispatch(goDown(index))
        }}
      >
        ▼ 아래로 이동
      </div>
    )
  }
}
UpButton.propTypes = {
  index: React.PropTypes.number.isRequired
}
DownButton = ReactRedux.connect()(DownButton)

class GoToButton extends React.Component {
  render () {
    let input
    const { index } = this.props
    return (
      <div className='row'>
            <div className='small-7 columns'>
              <input
                type='text'
                ref={(node) => { input = node }}
                placeholder='순서 입력 후 이동 클릭'
              />
            </div>
            <div className='small-5 columns'>
              <button
                className='tiny expand button'
                // onClick={() => {console.log(input.value, index)}}
                onClick={() => {
                  store.dispatch(goTo(index, input.value))
                  input.value=''
                }}
              >
                이동
              </button>
            </div>
      </div>
    )
  }
}
GoToButton.propTypes = {
  // onGoToButtonClick: React.PropTypes.func.isRequired
  index: React.PropTypes.number.isRequired
}
GoToButton = ReactRedux.connect()(GoToButton)


class AImage extends React.Component {
  render () {
    let imgStyle = {
      width: 100
    }
    const { index, url } = this.props
    const urlArray = url.split("/")
    const fileName = urlArray[urlArray.length -1]
    return (
      <div className='row panel'>
        <div className='small-2 columns'>
        <h4>#. {index}</h4>
        <p>{fileName}</p>
        <img
          src={url}
          style={imgStyle}
        />
        </div>
        <div className='small-10 columns'>
          <div className='row'>
            <div className='small-3 columns'>
              <UpButton
                index={index}
              />
            </div>
            <div className='small-3 columns'>
              <DownButton
                index={index}
              />
            </div>
            <div className='small-6 columns'>
              <GoToButton
                index={index}
            />
            </div>
          </div>
        </div>
      </div>
    )
  }
}


/****************************
 get images via ajax
****************************/

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  // return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  return (/^(HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});

function getImages(){
  var itemId = document.getElementById("end-container").getAttribute("data-itemId");
  var url = "/api/items/" + itemId + "/images"
  return $.ajax({
    url: url,
    dataType: 'json',
    cache: false,
    success: function(data) {
      store.dispatch(receiveImages(data))
      store.dispatch(setItemId(itemId))
    },
    error: function(xhr, status, err) {
      console.error(this.props.url, status, err.toString());
      alert('권한이 없습니다.');
    }
  });
}

function setImages(orders) {
  var itemId = document.getElementById("end-container").getAttribute("data-itemId");
  let url = '/item/' + itemId + '/images/'
  let images = store.getState().images
  var imageResult = images.map(image => {
    // console.log(image.order);
    console.log(image.id);
    return image.id
  })
  console.log(imageResult);
  $.post(
    url,
    {
      itemId: itemId,
      imageResult
    },
    function (data) {
      if (data['status'] == 'ok'){
        alert('저장되었습니다.');
      } else {
        alert('오류가 발생하였습니다');
      }
    }
  );
}
/*
 * Container Component
*/

class ImageList extends React.Component {

  componentDidMount() {
    getImages();
  }

  render () {
    const { images } = this.props
    var nodes = images.map((image, index) =>
        <AImage
          key={image.id}
          url={image.image}
          index={index + 1}
        />
      )
    return (
      <div>
        {nodes}
        <div
            className="button success"
            onClick={() => setImages()}
        >
          저장하기
        </div>
      </div>
    )
  }
}


/********
 * Store
 ********/
const mapStateToProps = (state) => {
  if (state.images.length == 0) {
    // console.log(getImages());
    state.images = []
    // console.log(state);
    return state
  } else {
    return state
  }
}

ImageList = ReactRedux.connect(
                        mapStateToProps
                      )(ImageList)

var store = Redux.createStore(
                    imageOrderApp,
                    Redux.applyMiddleware(ReduxThunk)
                  );

ReactDOM.render(
  <ReactRedux.Provider store={store}>
    <ImageList />
  </ReactRedux.Provider>,
  document.getElementById('image-list')
)
