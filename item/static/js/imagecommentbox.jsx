// 댓글 등록을 위한 AJAX 세팅
// ToDo: 해당 글 등록도 REST를 통하도록 수정해야함
var csrftoken = $.cookie('csrftoken');
function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});


var ImageCommentBox = React.createClass({
  getInitialState: function() {
    return {data: []}
  },
  componentDidMount: function() {
    this.loadImageCommentFromServer();
  },
  loadImageCommentFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('권한이 없습니다.');
      }.bind(this)
    });
  },
  handleCommentDelete: function(id) {
    // console.log('삭제할 아이디는 ' + id + '입니다.(from ImageCommentBox)');
    // console.log('삭제를 진행합니다.(from ImageCommentBox)');
    var url = '/api/image_comments/' + id + '.json';
    // console.log('삭제할 url은 ' + url + '입니다.');
    $.ajax({
      url: url,
      method: 'DELETE',
      dataType: 'json',
      cache: false,
      success: function() {
        this.loadImageCommentFromServer();
        // console.log('삭제 완료(from ImageCommentBox)');
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('댓글의 작성자만 삭제 할 수 있습니다.');
      }.bind(this)
    });
  },
  handleCommentUpdate: function(id, text) {
    // console.log('수정을 진행합니다.(from ImageCommentBox)');
    // console.log('아이디: ' + id +'\n문구: ' + text);
    var url = '/api/image_comments/' + id;
    $.ajax({
      url: url,
      method: 'PATCH',
      data: {text: text},
      cache: false,
      success: function(data) {
        // console.log(data);
        this.loadImageCommentFromServer();
        // console.log('수정 완료(from ImageCommentBox)');
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('댓글의 작성자만 수정 할 수 있습니다.');
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    var text = comment.text;
    var id = this.props.imageId;
    var data = this.state.data;
    var load = this.loadImageCommentFromServer;
    $.post(
      // '{% url 'item:add_comment' %}',
      '/item/add_comment',
      {
        id: id,
        text: text
      },
      function (data) {
        if (data['status'] == 'ok'){
          load();
        } else {
          alert('오류가 발생하였습니다');
        }
      }
    );
    // comment.id = Date.now();
    // comment.created = Date.now();
    // comment.created = Date.now();
    // comment.owner = '나';
    // comment.text = text;
    // var newComments = data.concat(comment);
    // this.setState({data: newComments});
  },
  render: function() {
    return (
      <div className="small-12 columns panel">
        <ImageCommentList data={this.state.data}
          onCommentUpdate={this.handleCommentUpdate}
          onCommentDelete={this.handleCommentDelete}
        />
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    );
  }
});
var ImageCommentList = React.createClass({
  // componentDidMount: function() {
  //   var onCommentDelete = this.props.onCommentDelete;
  //   console.log('ImageCommmentList에서 실행된 코드입니다.');
  //   this.props.onCommentDelete();
  // },
  render: function() {
    var onCommentDelete = this.props.onCommentDelete;
    var onCommentUpdate = this.props.onCommentUpdate;
    var commentNodes = this.props.data.map(function(comment) {
      return (
      <Comment
        data={comment}
        key={comment.id}
        onCommentDelete={onCommentDelete}
        onCommentUpdate={onCommentUpdate}
      />
      );
    });
    return (
    <div className="row">
      {commentNodes}
    </div>
    );
  }
});
var Comment = React.createClass({
  getInitialState: function() {
    return {
        editing: false,
        text: ''
    }
  },
  handleDelete: function () {
    // console.log('삭제버튼이 클릭되었어요');
    if (confirm('정말로 삭제하시겠습니까?')) {
      // console.log('삭제 승인되었음');
      this.props.onCommentDelete(this.props.data.id);
    } else {
      // console.log('삭제 취소되었음');
    }
  },
  handleStartEditing: function () {
    // console.log(this.props.data.text);
    this.setState({text: this.props.data.text, editing: true});
    // console.log(this.state);
    // console.log('수정을 시작합니다.');
    // this.setState({editing: true});
  },
  handleTextChange: function (e) {
    this.setState({text: e.target.value});
  },
  handleCancelEditing: function () {
    this.setState({editing: false});
  },
  handleUpdate: function () {
    this.props.onCommentUpdate(this.props.data.id, this.state.text);
    this.setState({editing: false});
  },
  render: function() {
    var created = new Date(this.props.data.created);
    var modified = new Date(this.props.data.modified);
    if (!this.state.editing) {
      return (
      <div className="small-12 columns">
        <span className="label">작성자: {this.props.data.owner}</span> {' '}
        <span className="label success">작성: {created.toString().slice(0, 24)}</span> {' '}
        <span className="label warning">수정: {modified.toString().slice(0, 24)}</span>
        <p>
          {this.props.data.text.split("\n").map(function(item) {
            return (<span>{item}<br /></span>)})
          }
        </p>
        <a
          className="tiny warning label"
          onClick={this.handleStartEditing}
        >수정</a>{' '}
        <a
          className="tiny alert label"
          onClick={this.handleDelete}
        >삭제</a>
        <hr />
      </div>
      )
    } else {
      var textareaSytle = {
        height: '150px',
        overflow: 'visible'
      };
      return (
        <div className="small-12 columns">
          <span className="label">작성자: {this.props.data.owner}</span> {' '}
          <span className="label success">작성: {created.toString().slice(0, 24)}</span> {' '}
          <textarea
            value = {this.state.text}
            style={textareaSytle}
            onChange={this.handleTextChange}
          />
          <a
            className="tiny alert label"
            onClick={this.handleCancelEditing}
          >취소</a>{' '}
          <a
            className="tiny success label"
            onClick={this.handleUpdate}
          >저장</a>
          <hr />
        </div>
      )
    }
  }
});
var CommentForm = React.createClass({
  getInitialState: function () {
    return {text: '', height: '10px'};
  },
  handleTextChange: function (e) {
    var nodeHeight = ReactDOM.findDOMNode(this.refs.theInput).scrollHeight;
    // console.log(nodeHeight);
    this.setState({text: e.target.value});
    this.state.height = (nodeHeight - 11 ) + 'px';
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var text = this.state.text.trim();
    if (!text) {
      alert('내용을 입력해 주세요!');
      return;
    }
    // alert('submit이 클릭되었습니다!!\n내용: ' + text);
    this.props.onCommentSubmit({text: text});
    this.setState({text: ''});
  },
  render: function () {
    var textareaSytle = {
      height: this.state.height,
      overflow: 'visible'
    };
    return (
    <form className="commentForm" onSubmit={this.handleSubmit}>
      <textarea
        ref="theInput"
        placeholder="코멘트를 입력해주세요"
        value={this.state.text}
        onChange={this.handleTextChange}
        style={textareaSytle}
      />
      <input
        type="submit"
        className="tiny button"
        value="입력하기"
      />
    </form>
    );
  }
  });

var comments = document.getElementsByName("comment");
for (var i = 0; i < comments.length; i++) {

  var itemId = comments[i].getAttribute("data-itemId");
  var imageId = comments[i].getAttribute("data-imageId");
  var url="/api/comments/" + imageId + ".json"
  // console.log(imageId);
  // console.log("item: ", itemId);

  ReactDOM.render(
    <ImageCommentBox
      url={url}
      itemId={itemId}
      imageId={imageId}
    />,
    comments[i]
  );

}
