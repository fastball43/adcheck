function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    // return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    return (/^(HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

Date.prototype.yyyymmdd = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();
    return yyyy + '.' + (mm[1] ? mm : '0'+mm[0]) + '.' + (dd[1] ? dd : '0'+dd[0]);
}

 class EndContainer extends React.Component {
  constructor() {
    super(...arguments);
    this.state = {
      images: [],
    };
  }

  componentDidMount() {
    this.loadImageFromServer();
  }

  loadImageFromServer() {
    let itemId = document.getElementById("end-container").getAttribute("data-itemId");
    // var url = "/api/images/" + itemId;
    var url = `/api/items/${itemId}/images`;
    $.ajax({
      url: url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({images: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('권한이 없습니다.');
      }.bind(this)
    });
  }

  deleteImage(imageId) {
    if (confirm('정말로 삭제하시겠습니까?')) {
      // console.log('삭제 승인되었음');
      let prevImages = this.state.images;
      let imageIndex = this.state.images.findIndex((image)=>image.id == imageId);
      let nextImages = prevImages.concat([]);
      nextImages.splice(imageIndex, 1);
      this.setState({images: nextImages});

      // let itemId = document.getElementById("end-container").getAttribute("data-itemId");
      // console.log(`You want to delete ID:${imageId} Image of ID:${itemId} Item`);
      var url = '/api/images/' + imageId + '.json';
      $.ajax({
        url: url,
        method: 'DELETE',
        dataType: 'json',
        cache: false,
        success: function() {
          // console.log('삭제 완료(from deleteImage of Container)');
        }.bind(this),
        error: function (xhr, status, err) {
          console.error(url, status, err.toString());
          alert('작성자만 삭제 할 수 있습니다.');
        }.bind(this)
      });
    } else {
      console.log('삭제 취소되었음');
    }
  }

  render() {
    let probe = document.getElementById("end-container");
    if (probe.getAttribute("data-isReadOnly")) {
      var viewOnly = false;
    } else {
      var viewOnly = true;
    }
    if (probe.getAttribute("data-isOwner")) {
      var isOwner = true;
    } else {
      var isOwner = false;
    }

    // console.log(viewOnly, isOwner);

    return (
      <div className="row">
        <ImageList
          taskCallbacks={
            { delete: this.deleteImage.bind(this), }
          }
          images={this.state.images}
          viewOnly={viewOnly}
          isOwner={isOwner}
        />
        </div>
    );
  }
}

class ImageList extends React.Component {
  render() {

    let images = this.props.images.map((image, i) => {
      // console.log(image, i);
      return (
        <ImageContainer
          key={i}
          imageUrl={image.image}
          imageOrder={image.order}
          imageId={image.id}
          taskCallbacks={this.props.taskCallbacks}
          viewOnly={this.props.viewOnly}
          isOwner={this.props.isOwner}

        />
      );
    });
    return (
      <div>
        {images}
      </div>
    )
  }
}

class ImageContainer extends React.Component {
  render() {
    // console.log(typeof(this.props.deleteImage));
    // console.log(this.props.imageId);
    // let url="/api/comments/" + this.props.imageId + ".json"
    let url=`/api/images/${this.props.imageId}/comments`
    // console.log(url);
    return (
      <div className="small-12 columns">
        <hr />
        {/*
        <p> hello I'm Image Container <br />
          my Image Url is {this.props.imageUrl} <br />
          my Image Id is {this.props.imageId}
        </p>
        */}
        <div className="small-8 columns">
          { this.props.isOwner ?
            <div>
              <span
                className="alert label"
                onClick={this.props.taskCallbacks.delete.bind(null, this.props.imageId)}
              > X 이미지 삭제 </span>{' '}
              {/*
                <span className="warning label">&#x2261; 이미지 순서 변경</span>
                */}
            </div>
            : null
          }
          <img src={this.props.imageUrl} />
        </div>
        <ImageCommentBox
            url={url}
            itemId={this.props.itemId}
            imageId={this.props.imageId}
            viewOnly={this.props.viewOnly}
        />
      </div>
    );
  }
}

var ImageCommentBox = React.createClass({
  getInitialState: function() {
    return {data: []}
  },
  componentDidMount: function() {
    this.loadImageCommentFromServer();
  },
  loadImageCommentFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('권한이 없습니다.');
      }.bind(this)
    });
  },
  handleCommentDelete: function(id) {
    // console.log('삭제할 아이디는 ' + id + '입니다.(from ImageCommentBox)');
    // console.log('삭제를 진행합니다.(from ImageCommentBox)');
    var url = '/api/image_comments/' + id + '.json';
    // console.log('삭제할 url은 ' + url + '입니다.');
    $.ajax({
      url: url,
      method: 'DELETE',
      dataType: 'json',
      cache: false,
      success: function() {
        this.loadImageCommentFromServer();
        // console.log('삭제 완료(from ImageCommentBox)');
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('댓글의 작성자만 삭제 할 수 있습니다.');
      }.bind(this)
    });
  },
  handleCommentUpdate: function(id, text) {
    // console.log('수정을 진행합니다.(from ImageCommentBox)');
    // console.log('아이디: ' + id +'\n문구: ' + text);
    var url = '/api/image_comments/' + id;
    $.ajax({
      url: url,
      method: 'PATCH',
      data: {text: text},
      cache: false,
      success: function(data) {
        // console.log(data);
        this.loadImageCommentFromServer();
        // console.log('수정 완료(from ImageCommentBox)');
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
        alert('댓글의 작성자만 수정 할 수 있습니다.');
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    var text = comment.text;
    var id = this.props.imageId;
    var data = this.state.data;
    var load = this.loadImageCommentFromServer;
    $.post(
      // '{% url 'item:add_comment' %}',
      '/item/add_comment',
      {
        id: id,
        text: text
      },
      function (data) {
        if (data['status'] == 'ok'){
          load();
        } else {
          alert('오류가 발생하였습니다');
        }
      }
    );
    // comment.id = Date.now();
    // comment.created = Date.now();
    // comment.created = Date.now();
    // comment.owner = '나';
    // comment.text = text;
    // var newComments = data.concat(comment);
    // this.setState({data: newComments});
  },
  render: function() {
    return (
      <div className="small-4 columns panel">
        <ImageCommentList data={this.state.data}
          onCommentUpdate={this.handleCommentUpdate}
          onCommentDelete={this.handleCommentDelete}
          viewOnly={this.props.viewOnly}
        />
        { this.props.viewOnly ?
          null :
          <CommentForm onCommentSubmit={this.handleCommentSubmit} />
        }
      </div>
    );
  }
});
var ImageCommentList = React.createClass({
  // componentDidMount: function() {
  //   var onCommentDelete = this.props.onCommentDelete;
  //   console.log('ImageCommmentList에서 실행된 코드입니다.');
  //   this.props.onCommentDelete();
  // },
  render: function() {
    var onCommentDelete = this.props.onCommentDelete;
    var onCommentUpdate = this.props.onCommentUpdate;
    var viewOnly = this.props.viewOnly
    if ( this.props.data[0] == undefined ) {
      var commentNodes = <div className="small-12 columns">
        <span>이슈 없음.</span>
        </div>
    } else {
      var commentNodes = this.props.data.map(function(comment) {
        return (
        <Comment
          data={comment}
          key={comment.id}
          onCommentDelete={onCommentDelete}
          onCommentUpdate={onCommentUpdate}
          viewOnly={viewOnly}
        />
        );
      });
    }
    return (
    <div className="row">
      {commentNodes}
    </div>
    );
  }
});
var Comment = React.createClass({
  getInitialState: function() {
    return {
        editing: false,
        text: ''
    }
  },
  handleDelete: function () {
    // console.log('삭제버튼이 클릭되었어요');
    if (confirm('정말로 삭제하시겠습니까?')) {
      // console.log('삭제 승인되었음');
      this.props.onCommentDelete(this.props.data.id);
    } else {
      // console.log('삭제 취소되었음');
    }
  },
  handleStartEditing: function () {
    // console.log(this.props.data.text);
    this.setState({text: this.props.data.text, editing: true});
    // console.log(this.state);
    // console.log('수정을 시작합니다.');
    // this.setState({editing: true});
  },
  handleTextChange: function (e) {
    this.setState({text: e.target.value});
  },
  handleCancelEditing: function () {
    this.setState({editing: false});
  },
  handleUpdate: function () {
    this.props.onCommentUpdate(this.props.data.id, this.state.text);
    this.setState({editing: false});
  },
  render: function() {
    var created = new Date(this.props.data.created).yyyymmdd() + ' ' + new Date(this.props.data.created).toLocaleTimeString();
    var modified = new Date(this.props.data.modified).yyyymmdd() + ' ' + new Date(this.props.data.created).toLocaleTimeString();
    if (!this.state.editing) {
      return (
      <div className="small-12 columns">
        { this.props.viewOnly ? null :
          <span className="label">작성자: {this.props.data.owner}</span>
        }
        {' '}<span className="label success">작성: {created}</span> {' '}
        <span className="label warning">수정: {modified}</span>
        <p>
          {this.props.data.text.split("\n").map(function(item, i) {
            return (<span key={i}>{item}<br /></span>)})
          }
        </p>
        { this.props.viewOnly ? null :
          <span>
            <a className="tiny warning label"
              onClick={this.handleStartEditing} >수정</a>
            <span>&nbsp;</span>
            <a className="tiny alert label"
              onClick={this.handleDelete} >삭제</a>
          </span>
        }
        <hr />
      </div>
      )
    } else {
      var textareaSytle = {
        height: '150px',
        overflow: 'visible'
      };
      return (
        <div className="small-12 columns">
          <span className="label">작성자: {this.props.data.owner}</span> {' '}
          <span className="label success">작성: {created.toString().slice(0, 24)}</span> {' '}
          <textarea
            value = {this.state.text}
            style={textareaSytle}
            onChange={this.handleTextChange}
          />
          <a
            className="tiny alert label"
            onClick={this.handleCancelEditing}
          >취소</a>{' '}
          <a
            className="tiny success label"
            onClick={this.handleUpdate}
          >저장</a>
          <hr />
        </div>
      )
    }
  }
});
var CommentForm = React.createClass({
  getInitialState: function () {
    return {text: '', height: '10px'};
  },
  handleTextChange: function (e) {
    var nodeHeight = ReactDOM.findDOMNode(this.refs.theInput).scrollHeight;
    // console.log(nodeHeight);
    this.setState({text: e.target.value});
    this.state.height = (nodeHeight - 11 ) + 'px';
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var text = this.state.text.trim();
    if (!text) {
      alert('내용을 입력해 주세요!');
      return;
    }
    // alert('submit이 클릭되었습니다!!\n내용: ' + text);
    this.props.onCommentSubmit({text: text});
    this.setState({text: ''});
  },
  render: function () {
    var textareaSytle = {
      height: this.state.height,
      overflow: 'visible'
    };
    return (
    <form className="commentForm" onSubmit={this.handleSubmit}>
      <textarea
        ref="theInput"
        placeholder="코멘트를 입력해주세요"
        value={this.state.text}
        onChange={this.handleTextChange}
        style={textareaSytle}
      />
      <input
        type="submit"
        className="tiny button"
        value="입력하기"
      />
    </form>
    );
  }
  });

var comments = document.getElementsByName("comment");
for (var i = 0; i < comments.length; i++) {

  var itemId = comments[i].getAttribute("data-itemId");
  var imageId = comments[i].getAttribute("data-imageId");
  var url="/api/comments/" + imageId + ".json"
  // console.log(imageId);
  // console.log("item: ", itemId);

  ReactDOM.render(
    <ImageCommentBox
      url={url}
      itemId={itemId}
      imageId={imageId}
    />,
    comments[i]
  );

}


ReactDOM.render(
  <EndContainer />,
  document.getElementById('end-container')
);


/*
 * action types
 */
const ADD_TODO = 'ADD_TODO'
const COMPLETE_TODO = 'COMPLETE_TODO'
const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'

/*
 * other constants
 */
 const VisibilityFilters = {
   SHOW_ALL: 'SHOW_ALL',
   SHOW_COMPLETE: 'SHOW_COMPLETE',
   SHOW_ACTIVE: 'SHOW_ACTIVE'
 }

 /*
  * action creators
  */
function addTodo(text) {
  return { type: ADD_TODO, text }
}

function completeTodo(index) {
  return { type: COMPLETE_TODO, index }
}

function setVisibilityFilter(filter) {
  return { type: SET_VISIBILITY_FILTER, filter }
}
