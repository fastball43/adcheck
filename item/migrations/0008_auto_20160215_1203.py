# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('item', '0007_auto_20160212_1721'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='작성일시')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='수정일시')),
            ],
            options={
                'ordering': ['created'],
                'verbose_name_plural': 'Reviews',
                'verbose_name': 'Review',
            },
        ),
        migrations.AlterModelOptions(
            name='item',
            options={'verbose_name': '광고 컨텐츠'},
        ),
        migrations.AddField(
            model_name='review',
            name='item',
            field=models.ForeignKey(related_name='reviews', to='item.Item'),
        ),
        migrations.AddField(
            model_name='review',
            name='user',
            field=models.ForeignKey(related_name='reviews', to=settings.AUTH_USER_MODEL, verbose_name='검수자'),
        ),
    ]
