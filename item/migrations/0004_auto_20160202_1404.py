# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('item', '0003_auto_20160202_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagecomment',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='작성일시'),
        ),
        migrations.AlterField(
            model_name='imagecomment',
            name='modified',
            field=models.DateTimeField(auto_now=True, verbose_name='수정일시'),
        ),
    ]
