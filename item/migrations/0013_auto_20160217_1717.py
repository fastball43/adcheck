# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('item', '0012_auto_20160215_1528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='disqulification',
            field=models.CharField(null=True, choices=[('A', '일반 표시광고 위반(허위/과대광고)'), ('B', '상표권/저작권 및 지적재산권 침해'), ('C', '상품정보제공고시 오류'), ('D', '자료 부족(필수제출 자료 누락)'), ('E', '정보 부족'), ('F', '광고금지상품')], max_length=50, blank=True, verbose_name='이슈분류코드'),
        ),
        migrations.AlterField(
            model_name='review',
            name='text',
            field=models.TextField(null=True, blank=True, verbose_name='심의총평'),
        ),
    ]
