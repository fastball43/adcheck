# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('item', '0002_auto_20160126_1639'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('text', models.TextField()),
                ('created', models.DateField(auto_now_add=True, verbose_name='작성일시')),
                ('modified', models.DateField(auto_now=True, verbose_name='수정일시')),
            ],
            options={
                'ordering': ['created'],
                'verbose_name_plural': 'ImageComments',
                'verbose_name': 'ImageComment',
            },
        ),
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['image'], 'verbose_name_plural': 'Images', 'verbose_name': 'Image'},
        ),
        migrations.AddField(
            model_name='imagecomment',
            name='image',
            field=models.ForeignKey(related_name='comments', to='item.Image'),
        ),
        migrations.AddField(
            model_name='imagecomment',
            name='user',
            field=models.ForeignKey(related_name='comments', to=settings.AUTH_USER_MODEL, verbose_name='작성자'),
        ),
    ]
