# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('item', '0009_auto_20160215_1356'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('rating', models.CharField(max_length=10, choices=[('A', '매우만족'), ('B', '만족'), ('C', '보통'), ('D', '불만족'), ('F', (('1', '답변 불성실'), ('2', '내용 불량'), ('3', '맘에 안들음'), ('4', '그냥'))), ('Z', '없음')], verbose_name='평가', blank=True)),
                ('item', models.ForeignKey(related_name='feedbacks', to='item.Item')),
                ('user', models.ForeignKey(related_name='feedbacks', verbose_name='평가자', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
