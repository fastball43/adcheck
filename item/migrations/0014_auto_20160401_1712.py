# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('item', '0013_auto_20160217_1717'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttachFile',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('file', models.FileField(upload_to='contents/files/%Y/%m/%d', null=True, blank=True)),
                ('item', models.ForeignKey(to='item.Item', related_name='attached_files')),
            ],
            options={
                'verbose_name': 'AttachFile',
                'verbose_name_plural': 'AttachFiles',
            },
        ),
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['order'], 'verbose_name': 'Image', 'verbose_name_plural': 'Images'},
        ),
    ]
