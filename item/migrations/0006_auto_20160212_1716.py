# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('item', '0005_item_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='status',
            field=models.CharField(default=1, choices=[(0, '0.심의대기중-수정중'), (1, '1.심의대기중'), (2, '2.심의중'), (3, '3.심의확인대기중'), (4, '4.심의확인중'), (5, '5.심의확인-반려됨'), (6, '6.심의완료됨-승인'), (7, '7.심의완료됨-조건부승인'), (8, '8.심의완료됨-광고불가')], verbose_name='심의진행단계', max_length=20),
        ),
    ]
