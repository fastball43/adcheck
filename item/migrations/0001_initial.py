# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('order', models.PositiveSmallIntegerField()),
                ('image', models.ImageField(blank=True, upload_to='contents/%Y/%m/%d')),
                ('text', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Images',
                'ordering': ['order'],
                'verbose_name': 'Image',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='광고 컨텐츠 제목(필수)')),
                ('created', models.DateTimeField(verbose_name='생성일시', auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='수정일시')),
                ('description', models.TextField(blank=True, null=True)),
                ('url', models.URLField(blank=True, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='작성자', related_name='items')),
            ],
            options={
                'verbose_name': '광고 컨텐츠',
            },
        ),
        migrations.AddField(
            model_name='image',
            name='item',
            field=models.ForeignKey(to='item.Item', related_name='images'),
        ),
    ]
