from django import forms
from .models import Item, Image, ImageComment, Review
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
# from django_file_form.forms import FileFormMixin, UploadedFileField
from django_file_form.forms import FileFormMixin, MultipleUploadedFileField


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        # fields = ('title', 'description', 'url', 'status')
        fields = ('title', 'description', 'url',)


class MultiFileInput(forms.FileInput):
    def render(self, name, value, attrs=None):
        attrs['multiple'] = 'multiple'
        return super(MultiFileInput, self).render(name, value, attrs)

    def value_from_datadict(self, data, files, name):
        if hasattr(files, 'getlist'):
            return files.getlist(name)
        else:
            value = files.get(name)
            if isinstance(value, list):
                return value
            else:
                return [value]


class MultiFileField(forms.FileField):
    widget = MultiFileInput
    default_error_messages = {
        'min_num': _(u'Ensure at least %(min_num)s files are uploaded \
            (received %(num_files)s).'),
        'max_num': _(u'Ensure at most %(max_num)s files are uploaded \
            (received %(num_files)s).'),
        'file_size': _(u'File %(uploaded_file_name)s exceeded maximum \
            upload size.'),
    }

    def __init__(self, *args, **kwargs):
        self.min_num = kwargs.pop('min_num', 0)
        self.max_num = kwargs.pop('max_num', None)
        self.maximum_file_size = kwargs.pop('maximum_file_size', None)
        super(MultiFileField, self).__init__(*args, **kwargs)

    def to_python(self, data):
        ret = []
        for item in data:
            ret.append(super(MultiFileField, self).to_python(item))
        return ret

    def validate(self, data):
        super(MultiFileField, self).validate(data)
        num_files = len(data)
        if len(data) and not data[0]:
            num_files = 0
        if num_files < self.min_num:
            raise ValidationError(
                self.error_messages['min_num'] %
                {'min_num': self.min_num, 'num_files': num_files}
            )
        if num_files > self.max_num:
            raise ValidationError(
                self.error_messages['max_num'] %
                {'max_num': self.max_num, 'num_files': num_files}
            )
        if num_files > 0:
            for uploaded_file in data:
                if uploaded_file.size > self.maximum_file_size:
                    raise ValidationError(
                        self.error_messages['file_size'] %
                        {'upload_file_name': uploaded_file.name}
                    )


class ImageForm(forms.Form):
    files = MultiFileField(
        max_num=20, min_num=0, maximum_file_size=1024*1024*5
    )


class ImageCommentForm(forms.ModelForm):
    class Meta:
        model = ImageComment
        fields = ('text',)


class ReviewForm(forms.Form):
    text = forms.CharField(
        widget=forms.Textarea,
        label='심의총평',
        required=False
    )
    disqulification = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=Review.DISQUALIFICATION_CHOICES,
        label='이슈 분류코드',
        required=False
    )


class NewImageForm(FileFormMixin, forms.Form):
    # input_file = UploadedFileField()
    input_file = MultipleUploadedFileField()


class AttachFileForm(FileFormMixin, forms.Form):
    input_file = MultipleUploadedFileField()
