from django.db import models
from django.conf import settings
import os


class Item(models.Model):
    title = models.CharField(verbose_name='광고 컨텐츠 제목(필수)', max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='items', verbose_name='작성자')
    created = models.DateTimeField(verbose_name='생성일시', auto_now_add=True)
    modified = models.DateTimeField(verbose_name='수정일시', auto_now=True)
    description = models.TextField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)

    STATUS_CHOICES = (
        ('0', '0.심의대기중-수정중'),
        ('1', '1.심의대기중'),
        ('2', '2.심의중'),
        ('3', '3.심의확인대기중'),
        ('4', '4.심의확인중'),
        ('5', '5.심의확인-반려됨'),
        ('6', '6.심의완료됨-승인'),
        ('7', '7.심의완료됨-조건부승인'),
        ('8', '8.심의완료됨-광고불가'),
    )
    status = models.CharField(
        verbose_name='심의진행단계', max_length=20, choices=STATUS_CHOICES,
        default='1'
    )

    def __str__(self):
        return self.title

    def status_name(self):
        # value.0: 내부용 문구, 1: 광고주 문구, 2: 상태별 class
        STATUS_NAMES = {
            '0': ['0.심의대기중-수정중', '심의대기중', 'secondary', 'secondary',
                  '등록하신 심의를 수정하고 계십니다.'],
            '1': ['1.심의대기중', '심의대기중', 'secondary', 'warning',
                  '심의진행순서를 대기중입니다.'],
            '2': ['2.심의중', '심의진행중', '', '',
                  '심의가 진행중입니다.'],
            '3': ['3.심의확인대기중', '심의진행중', '', '',
                  '심의가 진행중입니다.'],
            '4': ['4.심의확인중', '심의진행중', '', '',
                  '심의가 진행중입니다.'],
            '5': ['5.심의확인-반려됨', '심의진행중', '', 'alert',
                  '심의가 진행중입니다.'],
            '6': ['6.심의완료됨-승인', '완료: 승인', 'success', 'success',
                  '이상 없이 바로 광고진행이 가능합니다.'],
            '7': ['7.심의완료됨-조건부승인', '완료: 조건부 승인', 'warning', 'success',
                  '수정사항 확인 및 수정 후 광고진행 바랍니다.'],
            '8': ['8.심의완료됨-광고불가', '완료: 광고 불가', 'alert', 'success',
                  '해당 광고는 온라인 광고가 불가능합니다.'],
        }
        return STATUS_NAMES[self.status]

    class Meta:
        verbose_name = "광고 컨텐츠"
        # ordering = ('-modified',)


class Image(models.Model):
    item = models.ForeignKey(Item, related_name='images')
    order = models.PositiveSmallIntegerField()
    image = models.ImageField(upload_to='contents/%Y/%m/%d', blank=True)
    text = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "Image"
        verbose_name_plural = "Images"
        ordering = ['order']
        # ordering = ['image']

    def __str__(self):
        return '{}: {}의 이미지'.format(self.id, self.item.title)


class AttachFile(models.Model):
    item = models.ForeignKey(Item, related_name='attached_files')
    file = models.FileField(upload_to='contents/files/%Y/%m/%d',
                            null=True, blank=True)

    class Meta:
        verbose_name = "AttachFile"
        verbose_name_plural = "AttachFiles"

    def __str__(self):
        # return 'Id:{} => {}의 이미지'.format(self.id, self.item.title)
        return (self.file.url)

    def filename(self):
        return os.path.basename(self.file.name)


class ImageComment(models.Model):
    image = models.ForeignKey(Image,
                              related_name='comments')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='comments', verbose_name='작성자')
    text = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True, verbose_name='작성일시')
    modified = models.DateTimeField(auto_now=True, verbose_name='수정일시')

    class Meta:
        verbose_name = "ImageComment"
        verbose_name_plural = "ImageComments"
        ordering = ['created']

    def __str__(self):
        return '{}님이 {}에 작성한 댓글: {}'.format(self.user, self.image, self.text)


class Review(models.Model):
    item = models.OneToOneField(Item, related_name='review',
                                blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='reviews',
                             verbose_name='검수자')
    text = models.TextField(blank=True, null=True,
                            verbose_name='심의총평')

    DISQUALIFICATION_CHOICES = (
        ('A', '일반 표시광고 위반(허위/과대광고)'),
        ('B', '상표권/저작권 및 지적재산권 침해'),
        ('C', '상품정보제공고시 오류'),
        ('D', '자료 부족(필수제출 자료 누락)'),
        ('E', '정보 부족'),
        ('F', '광고금지상품'),
    )
    disqulification = models.CharField(
        choices=DISQUALIFICATION_CHOICES,
        max_length=50,
        null=True,
        blank=True,
        verbose_name='이슈분류코드'
    )

    created = models.DateTimeField(auto_now_add=True, verbose_name='작성일시')
    modified = models.DateTimeField(auto_now=True, verbose_name='수정일시')

    class Meta:
        verbose_name = "Review"
        verbose_name_plural = "Reviews"
        ordering = ['created']

    def disqulification_text(self):
        import re
        p = re.compile('[A-Z]')
        results = p.findall(self.disqulification)

        codes = {
            'A': '일반 표시광고 위반(허위/과대광고)',
            'B': '상표권/저작권 및 지적재산권 침해',
            'C': '상품정보제공고시 오류',
            'D': '자료 부족(필수제출 자료 누락)',
            'E': '정보 부족',
            'F': '광고금지상품',
        }

        if results:
            code_text = ''
            for result in results:
                code_text += codes[result] + ', '
            return '{}'.format(code_text)
        else:
            return '해당되는 이슈가 없습니다.'


class Feedback(models.Model):
    item = models.ForeignKey(Item, related_name='feedbacks')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='feedbacks',
                             verbose_name='평가자')
    RATING_CHOICES = (
        ('A', '매우만족'),
        ('B', '만족'),
        ('C', '보통'),
        ('D', '불만족'),
        ('F', (
                ('1', '답변 불성실'),
                ('2', '내용 불량'),
                ('3', '맘에 안들음'),
                ('4', '그냥'),
            ),
         ),
        ('Z', '없음'),
    )
    rating = models.CharField(max_length=10,
                              blank=True,
                              choices=RATING_CHOICES,
                              # default='Z',
                              verbose_name='평가')


# class History(models.Model):

#     class Meta:
#         verbose_name = "History"
#         verbose_name_plural = "Histories"

#     def __str__(self):
#         pass
