from django.db import models
from django.forms import TextInput
from django.contrib import admin
from .models import Item, Image, ImageComment, Review, Feedback, AttachFile


class ItemAdmin(admin.ModelAdmin):
    '''
        Admin View for Item
    '''
    list_display = ('title', 'user', 'description', 'status',
                    'modified', 'created',)
    list_filter = ('status',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    search_fields = ('title', 'description',)

admin.site.register(Item, ItemAdmin)


class ImageAdmin(admin.ModelAdmin):
    '''
        Admin View for Image
    '''
    list_display = ('id', 'image', 'text',)
    # list_filter = ('',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ('',)

admin.site.register(Image, ImageAdmin)


class ImageCommentAdmin(admin.ModelAdmin):
    '''
        Admin View for ImageComment
    '''
    list_display = ('id', 'user', 'image', 'text',)
    # list_filter = ('id')
    # inlines = [
    #     Inline,
    # ]
    search_fields = ('text', 'user',)

admin.site.register(ImageComment, ImageCommentAdmin)


class ReviewAdmin(admin.ModelAdmin):
    '''
        Admin View for Review
    '''
    list_display = ('id', 'user', 'text', 'item', 'created', 'modified',)
    list_filter = ('user',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    search_fields = ('text', 'user',)
    formfield_overrides = {
        models.CharField: {'widget': TextInput},
    }

admin.site.register(Review, ReviewAdmin)


class FeedbackAdmin(admin.ModelAdmin):
    '''
        Admin View for Feedback
    '''
    list_display = ('item', 'user', 'rating',)
    list_filter = ('rating',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ('',)

admin.site.register(Feedback, FeedbackAdmin)


class AttachFileAdmin(admin.ModelAdmin):
    '''
        Admin View for AttachFile
    '''
    list_display = ('id', 'file',)
    # list_filter = ('',)
    # inlines = [Inline, ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ('',)

admin.site.register(AttachFile, AttachFileAdmin)