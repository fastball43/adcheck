from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from .models import Item, Image, ImageComment, Review, AttachFile
from .forms import ItemForm, ImageForm, ImageCommentForm, ReviewForm
from .forms import NewImageForm, AttachFileForm
# for AJAX view
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.core.urlresolvers import reverse


@login_required
def new(request):
    if request.method == 'POST':
        item_form = ItemForm(request.POST)
        new_image_form = NewImageForm(request.POST)
        if item_form.is_valid():
            new_item = item_form.save(commit=False)
            new_item.user = request.user
            new_item.save()
            if new_image_form.is_valid():
                new_file_list = new_image_form.cleaned_data['input_file']
                print(new_file_list)
                new_order = 1
                for afile in new_file_list:
                    image = Image.objects.create(
                                item=new_item, order=new_order, image=afile
                            )
                    new_order += 1
                new_image_form.delete_temporary_files()
            else:
                print('image form is not valid!!')

            return redirect(reverse(
                                'item:detail',
                                kwargs={'item_id': new_item.id})
                            )
        else:
            return render(request, 'item/new.html',
                          {'item_form': item_form,
                           'new_image_form': new_image_form}
                          )

    else:
        item_form = ItemForm()
        new_image_form = NewImageForm()

        return render(request,
                      'item/new.html', {
                           'item_form': item_form,
                           'new_image_form': new_image_form}
                      )


@login_required
def detail(request, item_id):
    item = get_object_or_404(Item, pk=item_id)
    qs = Review.objects.filter(item_id=item_id)
    if qs.exists():
        data = {'text': item.review.text,
                'disqulification': item.review.disqulification}
        review_form = ReviewForm(initial=data)
    else:
        review_form = ReviewForm()
    context = {
        'item': item,
        'review_form': review_form,
        # 'is_new': is_new
    }
    return render(request, 'item/detail.html', context)


@login_required
def end(request, item_id):
    item = get_object_or_404(Item, pk=item_id)
    qs = Review.objects.filter(item_id=item_id)
    if qs.exists():
        data = {'text': item.review.text,
                'disqulification': item.review.disqulification}
        review_form = ReviewForm(initial=data)
    else:
        review_form = ReviewForm()
    new_image_form = NewImageForm()
    context = {
        'item': item,
        'review_form': review_form,
        'new_image_form': new_image_form
        # 'is_new': is_new
    }
    return render(request, 'item/end.html', context)


@login_required
def end_addimages(request, item_id):
    if request.method == 'POST':
        new_image_form = NewImageForm(request.POST)
        if new_image_form.is_valid():
            new_file_list = new_image_form.cleaned_data['input_file']
            item = get_object_or_404(Item, pk=item_id)
            images = item.images.all()
            order = 1
            for aimage in images:
                aimage.order = order
                aimage.save()
                order += 1
            for afile in new_file_list:
                image = Image.objects.create(
                            item=item, order=order, image=afile)
                order += 1
            new_image_form.delete_temporary_files()
    return redirect(reverse('item:detail', kwargs={'item_id': item_id}))


@login_required
def end_addfiles(request, item_id):
    if request.method == 'POST':
        attach_file_form = AttachFileForm(request.POST)
        if attach_file_form.is_valid():
            attach_file_list = attach_file_form.cleaned_data['input_file']
            item = get_object_or_404(Item, pk=item_id)
            for afile in attach_file_list:
                afile = AttachFile.objects.create(
                            item=item, file=afile)
            attach_file_form.delete_temporary_files()
    return redirect(reverse('item:detail', kwargs={'item_id': item_id}))


@login_required
@require_POST
def add_comment(request):
    image_id = request.POST.get('id')
    text = request.POST.get('text')
    if image_id and text:
        # print('포스트 정보를 정상적으로 받았습니다.')
        # print(image_id)
        # print(text)
        # print(request.user)
        image_comment_form = ImageCommentForm(request.POST)
        image_comment_form.text = text
        # print(image_comment_form.text)
        if image_comment_form.is_valid():
            cd = image_comment_form.cleaned_data
            image_comment_form.text = cd['text']
            # print('image_comment_form.is_valid():')
            # print('POSTed text is', image_comment_form.text)
            # print('Yes real vailidated!!')
            new_image_comment = image_comment_form.save(commit=False)
            new_image_comment.image = Image.objects.get(pk=image_id)
            new_image_comment.user = request.user
            image_comment_form.save()
            # print(new_image_comment)
            return JsonResponse({'status': 'ok'})
    return JsonResponse({'status': 'ko'})


@login_required
def review_list(request, user=None, status=None):
    qs = request.GET
    if 'status' in qs:
        status = qs['status']
    if 'user' in qs:
        user = qs['user']
    if status == 'None' and user == 'None':
        item_list = Item.objects.all()
    elif status == 'None' and user:
        item_list = Item.objects.filter(user__username=user)
    elif status and user == 'None':
        item_list = Item.objects.filter(status=status)
    elif status and user:
        item_list = Item.objects.filter(status=status, user__username=user)
    else:
        item_list = Item.objects.all()
    if request.user.profile.user_level == 'reviewer':
        item_list = item_list.exclude(status='0').exclude(status='6')\
            .exclude(status='7').exclude(status='8')
    context = {
        'item_list': item_list,
        'status': status,
        'user': user
    }
    return render(request, 'item/review_list.html', context)


@login_required
def review_update(request, item_id):
    if request.method == 'POST':
        level = request.user.profile.user_level
        print(level)
        if level == 'staff' or level == 'reviewer':
            print('권한 확인됨')
            review = ReviewForm(request.POST)
            if review.is_valid():
                print('폼은 유효합니다.')
                cd = review.cleaned_data
                qs = Review.objects.filter(item_id=item_id)
                # 리뷰 저장하기
                if qs.exists():
                    print('업데이트로 진행합니다.')
                    # 업데이트 하기
                    qs.update(
                        text=cd['text'],
                        user=request.user,
                        disqulification=cd['disqulification']
                    )
                    print('업데이트 되었습니다!!!!!')
                else:
                    Review.objects.create(
                        text=cd['text'],
                        disqulification=cd['disqulification'],
                        user=request.user,
                        item=Item.objects.get(pk=item_id)
                    )
                    print('리뷰를 신규 생성하였습니다 :))')
                # 광고 아이템 상태값 변경하기
                aitem = Item.objects.get(pk=item_id)
                print('아이템 상태: ', aitem.status)
                if aitem.status == '1' or aitem.status == '2':
                    aitem.status = '3'
                    aitem.save()
            else:
                print('폼이 유효하지 않습니다.')
            user = request.user
            print(user)
            context = {
                'review': Review.objects.get(item_id=item_id),
                'item_id': item_id,
                'aitem': aitem
            }
            return render(request,
                          'item/review_update_confirm.html',
                          context)

        else:  # 유저레벨이 관리자나 리뷰어가 아닌경우
            return redirect('dashboard')
    else:  # POST이외의 방식으로 접근하는 경우
        return redirect('dashboard')


@login_required
def history(request):
    level = request.user.profile.user_level
    print(level)
    if level == 'staff' or level == 'reviewer':
        # comment_list = ImageComment.objects.filter(user=request.user)
        comment_list = Item.objects.filter(
                            images__comments__user=request.user).distinct()
        review_list = Item.objects.filter(
                            review__user=request.user).distinct()

        context = {
            'comment_list': comment_list,
            'review_list': review_list
        }
        return render(request,
                      'item/history.html',
                      context)
    else:
        return redirect('dashboard')


@login_required
def review_complete(request, item_id, status_code):
    if request.method == 'GET':
        level = request.user.profile.user_level
        print(level)
        if level == 'staff':
            print('권한 확인됨')
            qs = Item.objects.filter(pk=item_id)
            if qs.exists():
                print('광고가 존재합니다.')
                if status_code in ['5', '6', '7', '8']:
                    qs.update(
                        status=status_code
                    )
                    print('업데이트 되었습니다!!!!!')
                    aitem = Item.objects.get(pk=item_id)
                    context = {
                        'review': Review.objects.get(item_id=item_id),
                        'item_id': item_id,
                        'aitem': aitem
                    }
                    return render(request,
                                  'item/review_update_confirm.html',
                                  context)
                else:  # 코드가 존재하지 않는 경우
                    print('적합하지 않은 코드입니다.')
                    return redirect('dashboard')
            else:  # 광고가 존재하지 않는 경우
                print('광고가 존재하지 않습니다.')
                return redirect('dashboard')
        else:  # 유저레벨이 관리자나 리뷰어가 아닌경우
            return redirect('dashboard')
    else:  # GET으로 접속하지 않은 경우
        print('GET이외의 방식으로 접속하였습니다.')
        return redirect('dashboard')


@login_required
def end_image_order(request, item_id):
    if request.method == 'GET':
        item = get_object_or_404(Item, pk=item_id)
        if (request.user == item.user or
                request.user.profile.user_level == 'staff'):
            context = {'item_id': item_id}
            return render(request,
                          'item/end_image_order.html',
                          context)
        else:
            return redirect('dashboard')

    elif request.method == 'POST':
        item = get_object_or_404(Item, pk=item_id)
        if (request.user == item.user or
                request.user.profile.user_level == 'staff'):
            item_id_from_request = request.POST.get('itemId')
            images = request.POST.getlist('imageResult[]')
            if (item_id == item_id_from_request and images):
                print(item_id, item_id_from_request)
                # print(request.POST)
                print(item_id, images)
                print(len(images))
                for index, image_id in enumerate(images):
                    try:
                        aimage = Image.objects.get(id=image_id)
                        aimage.order = index + 1
                        aimage.save()
                    except:
                        pass
                return JsonResponse({'status': 'ok'})
        return JsonResponse({'status': 'ko'})

    else:
        return redirect('dashboard')
